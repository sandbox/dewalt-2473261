<?php

/**
 * @file
 * API documentation for Commerce Gift.
 */

/**
 * Allows alter the gifts list, provided for product.
 *
 * @param array $gifts
 *   The list of gift nodes, keyed by node id.
 * @param object $node
 *   The product node object.
 * @param int $product_id
 *   The id of product entity.
 *
 * @see commerce_gift_form_commerce_cart_add_to_cart_form_alter()
 */
function hook_commerce_gift_nodes_list_alter(array &$gifts, $node, $product_id) {
  // Extend gifts list.
  if ($add_gift = node_load(variable_get('my_project_additional_gift_nid'))) {
    $gifts[$add_gift->nid] = $add_gift;
  }
  // Limit gifts count.
  $display_limit = variable_get('my_project_gifts_display_limit', 10);
  if (count($gifts) > $display_limit) {
    $gifts = array_slice($gifts, 0, $display_limit);
  }
}
