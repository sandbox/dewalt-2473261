-- SUMMARY --

The module provides ability to present gifts with products.

-- INSTALLATION --

Enable the module. New content type 'gift' with product reference field will be
created. Also, gift reference field will be added to all content types, which
have product reference field.

-- CONFIGURATION --

The module doesn't add permissions. Currently, configuration UI isn't
implemented.
The next variables are implemented for configuration:
- commerce_gift_element_type: allows configure form element to choose gift.
  Possible values are: radios, checkboxes, select.
- commerce_gift_show_rendered: renders gift node with teaser view mode instead
  of radio/checkbox label. Needs styling. (wasn't tested with 'select' element
  type).

-- USAGE --

Create gifts, assign it to product with 'Gifts' field in product node edit
interface. Options to choose gift will be shown on 'Add to Cart form' widget
in node full view.

-- ADMINISTRATION --

Gifts management is implemented for order edit form. Note: gift, which is added
with order edit form in administration area, can be attached to any product in
the order.

-- LIMITATIONS --

Only one gift per product item in cart is allowed in current version. The
configuration of gifts count per product item will be implemented later.
